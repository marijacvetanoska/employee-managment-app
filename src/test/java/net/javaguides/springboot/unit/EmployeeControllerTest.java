package net.javaguides.springboot.unit;

import net.javaguides.springboot.controller.EmployeeController;
import net.javaguides.springboot.model.Employee;
import net.javaguides.springboot.service.EmployeeService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.junit.Test;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles(profiles = "test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
@SpringBootTest
@AutoConfigureMockMvc
public class EmployeeControllerTest {

    @Autowired
    private MockMvc mockMvc;

//    @InjectMocks
//    private EmployeeController employeeController;

    @Autowired
    @Mock
    private EmployeeService employeeService;

    @BeforeEach
    public void setUp(WebApplicationContext wac) {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(wac).build();
    }

    @Test
    public void showNewEmployeeForm() throws Exception {
        Employee employee = new Employee();
        employee.setLastName("tmp");
        employee.setEmail("tmp@tmp.com");
        employee.setFirstName("temp");

        this.mockMvc.perform(get("/showNewEmployeeForm/")
                        .param("email", employee.getEmail())
                        .param("firstName", employee.getFirstName())
                        .param("lastName", employee.getLastName())
                        .sessionAttr("employee", employee))
                .andExpect(forwardedUrl("/"))
                .andExpect(model().hasErrors());
        Assertions.assertNotNull(employee,"Employee created");

    }

    @Test
    public void saveEmployee() throws Exception {
        Employee employee = new Employee();
        employee.setLastName("tmp");
        employee.setEmail("tmp@tmp.com");
        employee.setFirstName("temp");
        this.employeeService.saveEmployee(employee);
        this.mockMvc.perform(post("/saveEmployee/")
                        .sessionAttr("employee", employee))
                .andExpect(forwardedUrl(null))
                .andExpect(model().hasErrors());

        Assertions.assertNull(this.employeeService.getEmployeeById(employee.getId()),"Employee has been saved");
    }

    @Test
    public void showFormForUpdate() throws Exception {
        Employee employee = this.employeeService.getEmployeeById(1);
        Employee employee2 = this.employeeService.getEmployeeById(1);
        this.mockMvc.perform(get("/showFormForUpdate/" + employee.getId())
                        .param("email", employee.getEmail())
                        .param("firstName", employee.getFirstName())
                        .param("lastName", employee.getLastName())
                        .sessionAttr("employee", employee))
                .andExpect(forwardedUrl("/"))
                .andExpect(model().hasErrors());
        Assertions.assertNotEquals(employee,employee2,"Employees are not equal");
    }

    @Test
    public void deleteEmployee() throws Exception {
        Employee employee = this.employeeService.getAllEmployees().get(0);
        this.mockMvc.perform(get("/deleteEmployee/" + employee.getId())
                        .sessionAttr("employee", employee))
                .andExpect(forwardedUrl("/"))
                .andExpect(model().hasErrors());

        Assertions.assertNotNull(employee,"Employee has been deleted");
    }
}