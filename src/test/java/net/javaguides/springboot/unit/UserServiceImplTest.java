package net.javaguides.springboot.unit;

import net.javaguides.springboot.dto.UserRegistrationDto;
import net.javaguides.springboot.model.Role;
import net.javaguides.springboot.model.User;
import net.javaguides.springboot.repository.UserRepository;
import net.javaguides.springboot.service.UserService;
import net.javaguides.springboot.service.UserServiceImpl;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Arrays;

@SpringBootTest
public class UserServiceImplTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private BCryptPasswordEncoder passwordEncoder;

    private UserService service;


    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        User user = new User("Petre", "Petreski", "petre.petreski@gmail.com", "Test123!", Arrays.asList(new Role("ROLE_USER")));
        Mockito.when(this.userRepository.save(Mockito.any(User.class))).thenReturn(user);
        Mockito.when(this.passwordEncoder.encode(Mockito.anyString())).thenReturn("password");

        this.service = Mockito.spy(new UserServiceImpl(this.userRepository, this.passwordEncoder));
    }

    @Test
    public void saveUser() {
        UserRegistrationDto dto = new UserRegistrationDto("Petre", "Petreski", "petre.petreski@gmail.com", "Test123!");
        User user = this.service.save(dto);
        Mockito.verify(this.service).save(dto);

        Assertions.assertNotNull(user,"User is null");
        Assertions.assertEquals(dto.getFirstName(), user.getFirstName(),"Names do not match");
        Assertions.assertEquals( dto.getLastName(), user.getLastName(), "Surnames do not match");
        Assertions.assertEquals(dto.getPassword(), user.getPassword(), "Passwords do not match");
        Assertions.assertEquals( dto.getEmail(), user.getEmail(), "Emails do not match");
    }

    @Test
    public void testNullUsername() {
        UserRegistrationDto dto = new UserRegistrationDto("Petre", "Petreski", null, "Test123!");
        Assert.assertThrows("No valid arguments",
                Exception.class,
                () -> this.service.save(dto));
        Mockito.verify(this.service).save(dto);
    }
    @Test
    public void testEmptyUsername() {
        UserRegistrationDto dto = new UserRegistrationDto("Petre", "Petreski", "", "Test123!");
        Assert.assertThrows("No valid arguments",
                Exception.class,
                () -> this.service.save(dto));
        Mockito.verify(this.service).save(dto);
    }
    @Test
    public void testNullPassword() {
        UserRegistrationDto dto = new UserRegistrationDto("Petre", "Petreski", "petre.petreski@gmail.com", null);
        Assert.assertThrows("No valid arguments",
                Exception.class,
                () -> this.service.save(dto));
        Mockito.verify(this.service).save(dto);
    }
    @Test
    public void testEmptyPassword() {
        UserRegistrationDto dto = new UserRegistrationDto("Petre", "Petreski", "petre.petreski@gmail.com", "");
        Assert.assertThrows("No valid arguments",
                Exception.class,
                () -> this.service.save(dto));
        Mockito.verify(this.service).save(dto);
    }

}