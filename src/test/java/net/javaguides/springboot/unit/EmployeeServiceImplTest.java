package net.javaguides.springboot.unit;


import net.javaguides.springboot.model.Employee;
import net.javaguides.springboot.repository.EmployeeRepository;
import net.javaguides.springboot.service.EmployeeService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@SpringBootTest
public class EmployeeServiceImplTest {

    MockMvc mockMvc;

    @Mock
    @Autowired
    private EmployeeRepository employeeRepository;

    @Mock
    @Autowired
    private EmployeeService employeeService;

    Employee employee;

    @BeforeEach
    public void setUp(WebApplicationContext wac) {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        employee = this.employeeService.getEmployeeById(1);
    }

    @Test
    public void getAllEmployees() {
        Assertions.assertNotNull(this.employeeService.getAllEmployees(), "There are no employees");
    }

    @Test
    public void saveEmployee() {
        Employee e = new Employee();
        e.setFirstName("Marc");
        e.setLastName("Zuckerberg");
        e.setEmail("marc@test.com");
        this.employeeService.saveEmployee(e);
        Employee employee1 = this.employeeService.getEmployeeById(e.getId());
        Assertions.assertNotNull(employee1,"Employee saved");
    }

    @Test
    public void getEmployeeById() {
        Assertions.assertNotNull(employee, "No employee found");
    }

    @Test
    public void cantGetEmployeeById() {
        Assertions.assertNull(this.employeeService.getEmployeeById(0), "No employee found");
    }

    @Test
    public void deleteEmployeeById() {
        Employee em = this.employeeService.getAllEmployees().get(0);
        this.employeeService.deleteEmployeeById(em.getId());
        em = this.employeeService.getEmployeeById(em.getId());
        Assertions.assertNull(em,"Employee deleted");
    }

}