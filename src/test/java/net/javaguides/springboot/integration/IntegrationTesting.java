package net.javaguides.springboot.integration;


import net.javaguides.springboot.config.SecurityConfiguration;
import net.javaguides.springboot.controller.EmployeeController;
import net.javaguides.springboot.model.Employee;
import net.javaguides.springboot.repository.EmployeeRepository;
import net.javaguides.springboot.service.EmployeeService;
import net.javaguides.springboot.service.EmployeeServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.Assert;
import org.springframework.web.context.WebApplicationContext;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;
import javax.servlet.ServletContext;
import java.util.ArrayList;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@WebAppConfiguration
public class IntegrationTesting {

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    @BeforeEach
    public void init() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
    }

    @Test
    public void servletInit() {
        ServletContext servletContext = webApplicationContext.getServletContext();

        Assertions.assertNotNull(servletContext);
        Assertions.assertTrue(servletContext instanceof MockServletContext);
    }

    @Test
    public void getHomePageTest() throws Exception {
        this.mockMvc.perform(get("http://localhost:8080/"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(view().name("index"));
    }

    @Test
    public void getLoginPageTest() throws Exception {
        this.mockMvc.perform(get("http://localhost:8080/login"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(view().name("login"));
    }

    @Test
    public void getRegistrationPageTest() throws Exception {
        this.mockMvc.perform(get("http://localhost:8080/registration"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(view().name("registration"));
    }

    @Test
    public void getNewEmployeePageTest() throws Exception {
        this.mockMvc.perform(get("http://localhost:8080/showNewEmployeeForm"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(view().name("new_employee"));
    }

    @Test
    public void updateEmployeePageTest() throws Exception {

        this.mockMvc.perform(get("http://localhost:8080/showFormForUpdate/{id}", "7"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(view().name("update_employee"));
    }
}
